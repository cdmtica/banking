package com.twinero.banking.repository;

import com.twinero.banking.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class UserRepositoryImpl implements UserRepository {

    private final EntityManager entityManager;

    @Autowired
    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public UserEntity findByEmailAndPassword(String email, String password) {
        Query query = entityManager.createNamedQuery("findUserByEmailAndPassword");
        query.setParameter("email", email);
        query.setParameter("password", password);
        return (UserEntity) query.getSingleResult();
    }

    @Override
    public UserEntity findBySession(String session) {
        Query query = entityManager.createNamedQuery("findUserBySession");
        query.setParameter("session", session);
        return (UserEntity) query.getSingleResult();
    }
}
