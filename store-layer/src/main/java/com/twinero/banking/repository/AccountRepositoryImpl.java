package com.twinero.banking.repository;

import com.twinero.banking.entities.Account;
import com.twinero.banking.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

public abstract class AccountRepositoryImpl implements AccountRepository {

    private final EntityManager entityManager;

    @Autowired
    protected AccountRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Account findByUserEntity(UserEntity user) {
        return entityManager.find(Account.class, user.getAccount().getId());
    }
}
