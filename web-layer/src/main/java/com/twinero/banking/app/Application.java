package com.twinero.banking.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.twinero.banking"})
@EnableJpaAuditing
@ComponentScan({"com.twinero.banking.service", "com.twinero.banking.controller"})
@EntityScan("com.twinero.banking.entities")
@EnableJpaRepositories("com.twinero.banking.repository")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
