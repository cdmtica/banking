package com.twinero.banking.builders;

import com.twinero.banking.entities.Account;
import com.twinero.banking.entities.UserEntity;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class UserBuilder {

    private String email = "cliente@twinero.com";
    private String password;
    private Account account;
    private String session;
    private Boolean logged;

    public UserBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withAccount(Account account) {
        this.account = account;
        return this;
    }

    public UserBuilder withSession(String session) {
        this.session = session;
        return this;
    }

    public UserBuilder withLogged(Boolean logged) {
        this.logged = logged;
        return this;
    }

    public UserEntity build(Random random) {
        fillMissingInfo(random);
        return buildUser();
    }

    private void fillMissingInfo(Random random) {
        if (password == null) password = RandomStringUtils.random(10, true, true);
        if (account == null) account = new AccountBuilder().build(random);
        if (session == null) session = RandomStringUtils.random(10, true, true);
        if (logged == null) logged = random.nextBoolean();
    }

    private UserEntity buildUser() {
        UserEntity user = new UserEntity();
        user.setEmail(email);
        user.setPassword(password);
        user.setAccount(account);
        user.setSession(session);
        user.setLogged(logged);
        return user;
    }
}
