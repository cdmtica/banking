package com.twinero.banking.builders;

import com.twinero.banking.entities.Account;
import com.twinero.banking.entities.Transaction;
import com.twinero.banking.entities.TransactionType;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

public class TransactionBuilder {

    private Timestamp date;
    private TransactionType transactionType;
    private BigDecimal amount;
    private Account account;

    public TransactionBuilder withDate(Timestamp date) {
        this.date = date;
        return this;
    }

    public TransactionBuilder withTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public TransactionBuilder withAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public TransactionBuilder withAccount(Account account) {
        this.account = account;
        return this;
    }

    public Transaction build(Random random) {
        fillMissingInfo(random);
        return buildAccount();
    }

    private void fillMissingInfo(Random random) {
        if (date == null) date = new Timestamp(new Date().getTime());
        if (transactionType == null) {
            List<TransactionType> transactionTypes = Collections.unmodifiableList(Arrays.asList(TransactionType.values()));
            transactionType = transactionTypes.get(random.nextInt(transactionTypes.size()));
        }
        if (amount == null) amount = BigDecimal.valueOf(random.nextDouble());
        if (account == null) account = new Account();
    }

    private Transaction buildAccount() {
        Transaction transaction = new Transaction();
        transaction.setDate(date);
        transaction.setTransactionType(transactionType);
        transaction.setAmount(amount);
        transaction.setAccount(account);
        return transaction;
    }
}
