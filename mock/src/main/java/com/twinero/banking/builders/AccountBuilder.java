package com.twinero.banking.builders;

import com.twinero.banking.entities.Account;
import com.twinero.banking.entities.Transaction;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class AccountBuilder {

    private BigDecimal money;
    private List<Transaction> transactions;

    public AccountBuilder withMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    public AccountBuilder withTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public Account build(Random random) {
        fillMissingInfo(random);
        return buildAccount();
    }

    private void fillMissingInfo(Random random) {
        if (money == null) money = BigDecimal.valueOf(random.nextDouble());
        if (transactions == null) transactions = Collections.singletonList(new TransactionBuilder().build(random));
    }

    private Account buildAccount() {
        Account account = new Account();
        account.setMoney(money);
        account.setTransactions(transactions);
        return account;
    }
}
