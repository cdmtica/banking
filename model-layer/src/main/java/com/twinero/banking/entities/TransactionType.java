package com.twinero.banking.entities;

public enum TransactionType {

    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw");

    private final String type;

    TransactionType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
