package com.twinero.banking.service;

import com.twinero.banking.builders.UserBuilder;
import com.twinero.banking.controller.LoginController;
import com.twinero.banking.entities.UserEntity;
import com.twinero.banking.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LoginControllerTest {

    @Mock
    private LoginService loginService;
    @Mock
    private UserRepository userRepository;
    private LoginController loginController;
    private UserEntity user;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        user = new UserBuilder().build(new Random());
        loginController = new LoginController(loginService);
        when(userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword())).thenReturn(user);
    }

    @Test
    public void doLogin() {
        when(loginService.doLogin(user)).thenCallRealMethod();
        String login = loginController.doLogin(user);
        assertNotNull(login);
    }

    @Test
    public void signUp() {
    }

    @Test
    public void generateBalance() {
    }

    @Test
    public void generateStatement() {
    }
}
