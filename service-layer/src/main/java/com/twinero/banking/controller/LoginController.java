package com.twinero.banking.controller;

import com.twinero.banking.entities.UserEntity;
import com.twinero.banking.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class LoginController {

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(@RequestBody UserEntity user) {
        return loginService.doLogin(user);
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public String signUp(@RequestBody UserEntity user) {
        return loginService.signUp(user);
    }

    @RequestMapping(value = "/balance", method = RequestMethod.POST)
    public String retrieveAccountBalance(@RequestBody UserEntity userEntity) throws Exception {
        return loginService.generateBalance(userEntity.getSession());
    }

    @RequestMapping(value = "/statement", method = RequestMethod.POST)
    public String retrieveAccountStatement(@RequestBody UserEntity userEntity) throws Exception {
        return loginService.generateStatement(userEntity.getSession());
    }
}
