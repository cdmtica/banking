package com.twinero.banking.controller;

import com.twinero.banking.exception.NotEnoughtMoneyException;
import com.twinero.banking.exception.UserNotLoggedException;
import com.twinero.banking.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/deposit-money", method = RequestMethod.POST)
    public String depositMoney(@RequestBody Map<String, String> request) {
        String session = request.get("session");
        String money = request.get("money");
        try {
            return accountService.depositMoney(session, new BigDecimal(money));
        } catch (Exception e) {
            return "No User Logged";
        }
    }


    @RequestMapping(value = "/withdraw-money", method = RequestMethod.POST)
    public String withdrawMoney(@RequestBody Map<String, String> request) throws Exception {
        String session = request.get("session");
        String money = request.get("money");
        try {
            return accountService.withdrawMoney(session, new BigDecimal(money));
        } catch (NotEnoughtMoneyException | UserNotLoggedException e) {
            return e.getMessage();
        }
    }
}
