package com.twinero.banking.service;

import com.twinero.banking.entities.Account;
import com.twinero.banking.entities.UserEntity;
import com.twinero.banking.repository.AccountRepository;
import com.twinero.banking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collections;

@Service
public class LoginService {

    private static final String USERNAME_OR_PASSWORD_INVALID = "Username or Password Invalid";
    private static final String WELCOME_MSG = "Welcome ";
    private static final String ACCOUNT_CREATED = "The account was successfully created";
    private static final String ERROR_CREATING_ACCOUNT = "Error creating account";
    private static final String SESSION = " Session: ";
    private static final String NO_USER_FOUND = "No User Found";
    private final UserRepository userRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public LoginService(UserRepository userRepository, AccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
    }

    public String doLogin(UserEntity user) {
        try {
            UserEntity userEntity = userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
            userEntity.generateSessionCode();
            userRepository.save(userEntity);
            return WELCOME_MSG + userEntity.getEmail() + SESSION + userEntity.getSession();
        } catch (Exception e) {
            return USERNAME_OR_PASSWORD_INVALID;
        }
    }

    public String signUp(UserEntity user) {
        try {
            Account account = new Account();
            account.setMoney(BigDecimal.ZERO);
            user.setAccount(account);
            userRepository.save(user);
            accountRepository.save(account);
            return ACCOUNT_CREATED;
        } catch (Exception e) {
            return ERROR_CREATING_ACCOUNT;
        }
    }

    public String generateBalance(String session) {
        UserEntity user = userRepository.findBySession(session);
        if (user != null) {
            return Collections.singletonList(user.getAccount().getTransactions()).toString();
        } else {
            return NO_USER_FOUND;
        }
    }

    public String generateStatement(String session) {
        UserEntity user = userRepository.findBySession(session);
        if (user != null) {
            return user.getAccount().toString() + Collections.singletonList(user.getAccount().getTransactions());
        } else {
            return NO_USER_FOUND;
        }
    }
}
