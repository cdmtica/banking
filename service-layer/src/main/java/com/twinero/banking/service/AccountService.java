package com.twinero.banking.service;

import com.twinero.banking.entities.Account;
import com.twinero.banking.entities.Transaction;
import com.twinero.banking.entities.TransactionType;
import com.twinero.banking.entities.UserEntity;
import com.twinero.banking.exception.NotEnoughtMoneyException;
import com.twinero.banking.exception.UserNotLoggedException;
import com.twinero.banking.repository.AccountRepository;
import com.twinero.banking.repository.TransactionRepository;
import com.twinero.banking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Service
public class AccountService {

    private static final String NO_USER_FOUND = "No User Found";
    private static final String INSUFFICIENT_BALANCE = "Insufficient Balance";
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, UserRepository userRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
    }

    public String depositMoney(String session, BigDecimal amount) throws Exception {
        UserEntity user = userRepository.findBySession(session);
        if (user != null) {
            Account account = user.getAccount();
            account.setMoney(account.getMoney().add(amount));
            saveTransaction(account, amount, TransactionType.DEPOSIT);
            return mergeAccount(account);
        } else {
            throw new UserNotLoggedException(NO_USER_FOUND);
        }
    }


    public String withdrawMoney(String session, BigDecimal amount) throws Exception {
        UserEntity user = userRepository.findBySession(session);
        if (user != null) {
            Account account = user.getAccount();
            if (account.getMoney().subtract(amount).compareTo(BigDecimal.ZERO) >= 0) {
                account.setMoney(account.getMoney().subtract(amount));
                saveTransaction(account, amount, TransactionType.WITHDRAW);
                return mergeAccount(account);
            } else {
                throw new NotEnoughtMoneyException(INSUFFICIENT_BALANCE);
            }
        } else {
            throw new UserNotLoggedException(NO_USER_FOUND);
        }
    }

    private void saveTransaction(Account account, BigDecimal amount, TransactionType transactionType) {
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setDate(new Timestamp(new Date().getTime()));
        transaction.setTransactionType(transactionType);
        transaction.setAmount(amount);
        transactionRepository.save(transaction);
    }

    private String mergeAccount(Account account) {
        try {
            accountRepository.save(account);
            return "Transaction Successful";
        } catch (Exception e) {
            return "Transaction Error";
        }
    }
}
