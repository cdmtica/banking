package com.twinero.banking.exception;

public class UserNotLoggedException extends Exception {

    public UserNotLoggedException(String messagge) {
        super(messagge);
    }
}
