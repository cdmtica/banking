package com.twinero.banking.exception;

public class NotEnoughtMoneyException extends Exception {

    public NotEnoughtMoneyException(String messagge) {
        super(messagge);
    }
}
