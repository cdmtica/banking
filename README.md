# **Banking**

## Rest Services
###Sign-Up
User shall Sign-up using `email` and `password`  
the following example is used to `Sign-Up` in the app.  
**URL:**
`http://localhost:8080/banking/user/sign-up`  
```
{
    "email": "cdmtica@gmail.com",
	"password": "32394231"
}
```

###Login
**URL:**
`http://localhost:8080/banking/user/login`  
The request used to login is the same as `Sign-up`
```
{
    "email": "cdmtica@gmail.com",
	"password": "32394231"
}
```

after the client send the request, a `session` parameter will be obtained,  
this parameter is needed to perform any other request, like, `deposit-money`, `withdraw-money`, `statemen` and `balance`. 

###**Transactions**
####Deposit
**URL:**
`http://localhost:8080/banking/account/deposit-money`
In order to perform a transaction, the user shall send apart of the amount of money to deposit, the `session` obtained 
from the `login` method.
```
{
	"session": "YXpBuwyDn0",
	"money": "300"
}
```

####Withdraw
**URL:**
`http://localhost:8080/banking/account/withdraw-money`
In order to perform a transaction, the user shall send apart of the amount of money to withdraw, the `session` obtained 
from the `login` method.
```
{
	"session": "YXpBuwyDn0",
	"money": "300"
}
```

###**Account**
###Statement
**URL:**
`http://localhost:8080/banking/user/statement`
```
{
	"session": "YXpBuwyDn0"
}
```

###Balance
**URL:**
`http://localhost:8080/banking/user/balance`
```
{
	"session": "YXpBuwyDn0"
}
```